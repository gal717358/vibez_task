import {
  Avatar,
  Flex,
  Table,
  Checkbox,
  Icon,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useColorModeValue,
} from "@chakra-ui/react";
import React, { useMemo, useState, useEffect } from "react";
import {
  useGlobalFilter,
  usePagination,
  useSortBy,
  useTable,
} from "react-table";

// Custom components
import Card from "components/card/Card";
import { normalizeDate } from "utils/datetime";
// Assets
// import { OrderPopup } from "./OrderPopup";
// import { ChangeStatusHandler } from "./ChangeStatusHanler";
import { currency_dict } from "dictionaries";
import { UseUnuseButton } from "components/buttons/UseUnuseButton";
import { PersonCell } from "components/popups/PersonCell";

export function ItemsTable(props) {
  const { columnsData, tableData, fetchOrderItems, user, event_path } = props;
  //   const [isOpen, setIsOpen] = useState(false);
  const [error, setError] = useState(null);
  //   const [index, setIndex] = useState(-1);

  const columns = useMemo(() => columnsData, [columnsData]);
  const data = useMemo(() => tableData, [tableData]);
  let hash = window.location.hash;

  const tableInstance = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter,
    useSortBy,
    usePagination
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    initialState,
  } = tableInstance;
  initialState.pageSize = 10;

  const textColor = useColorModeValue("secondaryGray.900", "white");
  const borderColor = useColorModeValue("gray.200", "whiteAlpha.100");

  //   const renderOrderPopup = (item) => {
  //     if (isOpen)
  //       return (
  //         <OrderPopup
  //           index={index}
  //           setIndex={setIndex}
  //           id={data[index]?.id}
  //           fetchOrders={() => fetchOrders()}
  //           user={user}
  //           person={data[index]?.person}
  //         />
  //       );
  //   };

  //   useEffect(() => {
  //     setIsOpen(index < 0 ? false : true);
  //     document.body.style.overflow = index < 0 ? "unset" : "hidden";
  //   }, [index]);

  const renderBody = () => {
    return (
      data &&
      data.map((item, index) => {
        return (
          <tr key={item.id}>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <PersonCell
                  user={user}
                  person={item?.person}
                  membership={item?.membership}
                />
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {item?.product?.name}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {currency_dict["ILS"]}
                  {item?.price}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {item?.used ? "USED" : "NOT USED"}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  #{item?.order?.number}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {normalizeDate(item?.scanned_at)}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <UseUnuseButton
                  isUsed={item?.used}
                  user={user}
                  action={fetchOrderItems}
                  setError={setError}
                  path={
                    event_path +
                    "/orders/" +
                    item?.order?.id +
                    "/items/" +
                    item.id
                  }
                />
              </Flex>
            </td>
          </tr>
        );
      })
    );
  };

  return (
    <Card
      direction="column"
      w="100%"
      px="0px"
      overflowX={{ sm: "scroll", lg: "hidden" }}
    >
      {error && (
        <Text color="red" mb={"10px"}>
          {error} HI
        </Text>
      )}
      {/* {renderOrderPopup()} */}
      <Table {...getTableProps()} variant="simple" color="gray.500" mb="24px">
        <Thead>
          {headerGroups.map((headerGroup, index) => (
            <Tr {...headerGroup.getHeaderGroupProps()} key={index}>
              {headerGroup.headers.map((column, index) => (
                <Th
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                  pe="10px"
                  key={index}
                  borderColor={borderColor}
                >
                  <Flex
                    justify="space-between"
                    align="center"
                    fontSize={{ sm: "10px", lg: "12px" }}
                    color="gray.400"
                  >
                    {column.render("Header")}
                  </Flex>
                </Th>
              ))}
            </Tr>
          ))}
        </Thead>
        <Tbody {...getTableBodyProps()}>{renderBody()}</Tbody>
      </Table>
    </Card>
  );
}
